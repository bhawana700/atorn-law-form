<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Theme;
use Illuminate\Database\Seeder;
use App\Models\Social;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

       
        Admin::insert([
            'name' => 'Bhawana Niroula',
            'email' => 'bhawananiroula12@gmail.com',
            'password' => bcrypt('password'),
        ]);

        Admin::insert([
            'name' => 'Admin User',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
        ]);

        Theme::insert([
            'website_name' => "Tech Coderz",
            'website_tagline' =>"Inspire the Next",
        ]);

        Social::insert([
            'facebook' => "",
           
        ]);
    }
}
